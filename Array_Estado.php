<?php

$EstadoCigla = array('ES',
					 'MG',
					 'RJ',
					 'SP',
					 'MT');

$EstadoNome = array('Espírito Santo',
					'Minas Gerais',
					'Rio de Janeiro',
					'São Paulo',
					'Mato Grosso');

$CiglaNome = array_combine($EstadoCigla, $EstadoNome);


foreach ($CiglaNome as $Cigla => $Nome) { 
        echo $Cigla." - ".$Nome."<br/>";
    }

