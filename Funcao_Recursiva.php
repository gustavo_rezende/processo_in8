<?php
function mdc($x,$y,$z){
	$a = max($x,$y,$z);
	$b = min($x,$y,$z);
	if($a%$b == 0){
		return $b;
	}else{
		return mdc($b,($a%$b));
	}
}


function mmc($x,$y, $z){
	return (($x*$y*$z)/(mdc($x,$y,$z)));
	

}

echo mmc(2,3,10);