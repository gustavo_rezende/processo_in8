<?php
class Carro {
  public $Modelo;
  public $Km;
  public $Cliente;
  
  
  public function getModelo() {
    return $this->Modelo;
  }
  
  public function setModelo($Modelo) {
    $this->Modelo= $Modelo;
  }
  
  
  public function getKm() {
    // return $this->Km;
    $KmVeiculo = $this->Km;

    if ($KmVeiculo <= 10000)
    {
    	return "troca de óleo";
    }
    else if($KmVeiculo >=1001 && $KmVeiculo <= 50000)
    {
    	return "troca de pastilhas de freio";
    }
    else
    {
    	return "troca de correia dentada";
    }
  }
  
  public function setKm($Km) {
    $this->Km = $Km;
  }
  
  
  public function getCliente() {
    return $this->Cliente;
  }
  
  public function setCliente($Nome) {
    $this->Cliente = $Nome;
  }
}

?>




<!DOCTYPE html>
<html>
<head>
	<title>Revisão Automobilistica</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<body>
	<div class="container">
		<form method="POST">
		  <div class="form-group">
		    <label >Modelo Veículo</label>
		    <input type="text" class="form-control" name="modelo" id="modelo" placeholder="Modelo do Veículo">    
		  </div>
		  <div class="form-group">
		    <label>Kilometragem Veículo</label>
		    <input type="number" class="form-control" name="kmveiculo" id="kmveiculo" placeholder="Kilometragem do Veículo">
		  </div>
		  <div class="form-group">
		    <label>Nome cliente</label>
		    <input type="text" class="form-control" name="nomecliente" id="nomecliente" placeholder="Nome do Cliente">
		  </div>  
		  <button type="submit" name="Enviar" class="btn btn-primary">Enviar</button>
		</form>	
	</div>
</body>
</html>


<?php


if (isset($_POST['Enviar'])){



$Carro = new Carro;

$modelo = $Carro->setModelo($_POST['modelo']);

$Km = $Carro->setKm($_POST['kmveiculo']);

$Dono = $Carro->setCliente($_POST['nomecliente']);

echo "<br> O Cliente {$Carro->getCliente()} trouxe seu veículo {$Carro->getModelo()} para serviço de {$Carro->getKm()}";

}